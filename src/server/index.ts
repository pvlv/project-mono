import * as path from 'path';
import * as express from 'express';
import * as serveStatic from 'serve-static';
import * as nunjucks from 'nunjucks';
import * as webpack from 'webpack';
import * as webpackDevMiddleware from 'webpack-dev-middleware';
import * as webpackHotMiddleware from 'webpack-hot-middleware';
import * as webpackConfig from '../../webpack.config.js';

const PORT                  = process.env.PORT || 2233;
const HOST                  = process.env.HOST || '0.0.0.0';
const isDev                 = process.env.NODE_ENV !== 'production';
const SHARED_DIR_PATH       = path.resolve(__dirname, '../../shared');
const SHARED_VIEWS_DIR_PATH = path.join(SHARED_DIR_PATH, 'views');
const ASSETS_DIR_PATH       = path.resolve(__dirname, '../assets');
const ASSETS_DIST_DIR_PATH  = path.resolve(__dirname, '../../dist/assets');
const VIEW_DIR_PATH         = path.resolve(__dirname, '../views');
const ASSETS_PATH_BY_ENV    = isDev ? ASSETS_DIR_PATH : ASSETS_DIST_DIR_PATH;

const app = express();
const envNunjucks = nunjucks.configure([
  VIEW_DIR_PATH,
  SHARED_VIEWS_DIR_PATH
], {
    express: app,
    noCache: isDev
})

app.set('view engine', 'njk');

if (isDev) {
  const compiler = webpack(webpackConfig);
  const middleware = webpackDevMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath,
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  });

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));
}

app.use('/assets', serveStatic(ASSETS_PATH_BY_ENV));
app.use('/shared', serveStatic(SHARED_DIR_PATH));
app.get('/hello', (req, res) => {
  res.render('hello');
});
app.get('*', (req, res) => {
  res.render('layout/layout');
});

app.listen(PORT, HOST, () => {
  console.log('==> Listening on port %s. Open up http://0.0.0.0:%s/.', PORT, PORT);
})