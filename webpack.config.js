'use strict';

var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'eval-source-map',
  entry: [
    path.resolve(__dirname, './src/client/index.js'),
    'webpack-hot-middleware/client?reload=true'
  ],
  output: {
    path: path.resolve(__dirname, './src/assets/js/'),
    filename: 'app.js',
    publicPath: '/assets/js'
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        "presets": ["react", "es2015", "stage-0", "react-hmre"]
      }
    },
    {
      test: /\.json?$/,
      loader: 'json'
    },
    {
      test: /\.css$/,
      loader: 'style-loader!css-loader?modules&localIdentName=[name]---[local]---[hash:base64:5]'
    }]
  },
  resolve: {
    alias: {
      Shared: path.resolve(__dirname, './shared')
    }
  }
};