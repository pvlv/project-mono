PROJECT_NAME := project_mono
APP_DIR := app
DCWORK_DIR := /data
WATCHLOG_DIR := watchlog/log
NODE_MODULES_DIR := node_modules
NODE_CACHE_DIR := $$HOME/.node6-cache:/root/.npm
NODE_DOCKER_IMAGE := node:6.5
# TARGETS_PATH_LOCAL := targets
TARGETS_PATH := $(NODE_MODULES_DIR)/make-targs/targets
PORT := 2233:2233

#! Help - see this reference
-include $(TARGETS_PATH)/help.mk

-include $(TARGETS_PATH)/docker_container.mk
docker_container: _docker_container

-include $(TARGETS_PATH)/watchlog.mk

#! Install - install all dependency
-include $(TARGETS_PATH)/npm_install.mk
install: _npm_install

#! In - run container with bash
-include $(TARGETS_PATH)/in.mk
in: _in

-include $(TARGETS_PATH)/exec.mk

#! Exec_in - exec in ranned container
-include $(TARGETS_PATH)/exec_in.mk
exec_in: _exec_in